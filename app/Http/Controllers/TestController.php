<?php

namespace App\Http\Controllers;

use App\Role;
use App\Test;
use App\Traits\FlashMessages;
use App\User;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TestController extends Controller
{

    public function __construct()
    {
//        $this->middleware('auth');
    }

    use FlashMessages;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
//        $test = Test::All()->toJson();
        $test = Test::All();

//        self::message('info', 'ok');
//        $request->session()->flash('messaggio', 'Tutti gli utenti!');
//        $request->session()->keep(['messaggio']);


        return View('test.index', compact('test'));




//            ->with('test', $test);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        //
//        echo $_POST['name'];
//        $decode = json_encode( $_POST['name']);
//        echo $decode;
        if ($request->isMethod('post')){


            $data = $request->all();
//            var_dump($data);
//            die();
            $user = new User($data);
            $user->name = $data['name'];
            $user->email = $data['email'];
            $password = hash('md5',$data['password']);
            $user->password = $password;
            $user->username = $data['username'];
            $user->status = 0;

            $user->save();


//            return response()->json([$data]);
        }
//        $data = $request->all();
//        var_dump($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = User::findOrFail($id);
        var_dump($user);
        return view('test.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
