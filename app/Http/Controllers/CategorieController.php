<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategorieController extends Controller
{
    //
    public function show($category, $slug)
    {
        $page = Page::where('category', $category)->where('slug', $slug);

        return view('pages.show')->withPage($page);
    }
}
