@extends('layouts.app')



@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @include('partials.messages')

                {{--@if(Auth::check())--}}
                {{--<ul>--}}
                    {{--@foreach($test as $key => $value)--}}
                        {{--<li>{{ $value->nome }} - <b>{{ $value->cognome }}</b> - {{ $value->indirizzo }}</li>--}}
                        {{--@endforeach--}}
                {{--</ul>--}}

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">First</th>
                        <th scope="col">Last</th>
                        <th scope="col">Address</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($test as $key => $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->nome }}</td>
                        <td>{{ $value->cognome }}</td>
                        <td>@{{ $value->indirizzo }}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>

                    {{--@else--}}
                    {{--<p>Mi dispiace per vedere il contenuto bisogna loggarsi</p>--}}
                    {{--@endif--}}


                <button type="button" id="ajax">Invia</button>

                <form method="post" action="/post-route?utm=12345">
                    {{ csrf_field() }}
                    <input type="text" name="firstName">
                    <input type="submit">
                </form>

            </div>
        </div>
    </div>
@endsection


{{--<script type="text/javascript">--}}

{{--</script>--}}