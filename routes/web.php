<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Illuminate\Support\Facades\Request;

use App\Articles\ArticlesRepository;

Route::get('/', function () {

    Redis::set('name', 'Peppe');

    return Redis::get('name');

//    return view('welcome');
});
Route::get('/form', function () {
    return view('form');
});

Route::get('/pdfview', function () {
    return view('pdfView');
});

Route::post('submitForm','UserDetailController@store');

Route::get('/index','UserDetailController@index');

Route::get('/downloadPDF/{id}','UserDetailController@downloadPDF');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/test', 'TestController@index');
//    ->middleware('auth');

Route::post("/testajax", 'TestController@create');
Route::get("/test/{id}/show", "TestController@show");

Route::get('/categorie/{commentId?}', function ($id){
//    return 'categoria ' . + !(empty($id))? "$id" : null ;
//    return redirect('home')->with(['error' => true, 'message' => 'Whoops!']);
    abort(403, 'Semplicemente NON Puoi!');
});

Route::post('/post-route', function (Request $request) {
    var_dump($request->all());
});

Route::get('/articles', function () {
    return view('articles.index', [
        'articles' => App\Article::all(),
    ]);
});

Route::get('/search', function (ArticlesRepository $repository) {
    $articles = $repository->search((string) request('q'));

    return view('articles.index', [
        'articles' => $articles,
    ]);
});
