<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 6000; $i++) {
            DB::table('test')->insert([ //,
                'nome' => $faker->firstName,
                'cognome' => $faker->lastName,
                'indirizzo' => $faker->address,
                'eta' => $faker->numberBetween(10, 80),
            ]);
        }

//        Model::unguard();

//        $this->call(Test::class);

//        Model::reguard();
    }
}
